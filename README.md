﻿# MiniOTP

A minimal, customisable HOTP and TOTP implementation.

MVP works in `#![no_std]` mode with `default-features = false`.
