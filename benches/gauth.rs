﻿const SHA1_VALUES: [(u64, u32); 6] = [
	(59, 287_082),
	(1111111109, 081_804),
	(1111111111, 050_471),
	(1234567890, 005_924),
	(2000000000, 279_037),
	(20000000000, 353_130),
];
const DATA: [u8; 20] = *b"12345678901234567890";
use miniotp::TOTP;
fn main() {
	for _ in 0..usize::MAX {
		for (time, input) in SHA1_VALUES.iter() {
			assert!(TOTP::new(DATA).verify(*time, *input));
		}
	}
}
