﻿/// A type that can be coerced to a byte slice.
pub trait Array {
	/// Coerce a type to a byte slice.
	fn as_bytes(&self) -> &[u8];
}
impl<T> Array for T
where
	T: AsRef<[u8]>,
{
	fn as_bytes(&self) -> &[u8] {
		self.as_ref()
	}
}
