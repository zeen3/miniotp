﻿//! # Algorithms
//!
//! The algorithms that are used in the OTP spec. Note that these are used only unless the `only-gauth` feature is enabled.
use ring::hmac::{
	Algorithm as HA,
	HMAC_SHA1_FOR_LEGACY_USE_ONLY,
	HMAC_SHA256,
	HMAC_SHA512,
};
/// The algorithm run for generating a HOTP or TOTP.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(
	all(feature = "abomonation_derive", feature = "abomonation_main"),
	derive(abomonation_derive::Abomonation)
)]
pub enum Algorithm {
	/// SHA1
	///
	/// The default algorithm, and also the only algorithm that is supported by some authenticator
	/// applications (namely Google Authenticator).
	SHA1,
	/// SHA256
	///
	/// An alternative algorithm for OTP authentication
	SHA256,
	/// SHA512
	///
	/// An alternative algorithm for OTP authentication
	SHA512,
}
pub use Algorithm::*;
impl Into<HA> for Algorithm {
	fn into(self) -> HA {
		match self {
			SHA1 => HMAC_SHA1_FOR_LEGACY_USE_ONLY,
			SHA256 => HMAC_SHA256,
			SHA512 => HMAC_SHA512,
		}
	}
}
impl core::fmt::Display for Algorithm {
	fn fmt(
		&self,
		f: &mut core::fmt::Formatter,
	) -> core::fmt::Result {
		core::fmt::Debug::fmt(self, f)
	}
}
#[cfg(test)]
mod test {
	use super::*;
	#[cfg(any(feature = "alloc", feature = "std"))]
	#[test]
	fn debug() {
		#[cfg(feature = "alloc")]
		use alloc::string::ToString;
		assert_eq!(SHA1.to_string(), "SHA1");
		assert_eq!(SHA256.to_string(), "SHA256");
		assert_eq!(SHA512.to_string(), "SHA512");

		#[cfg(feature = "std")]
		{
			assert_eq!(format!("{:?}", SHA1), "SHA1");
			assert_eq!(format!("{:?}", SHA256), "SHA256");
			assert_eq!(format!("{:?}", SHA512), "SHA512");
		}
	}
	#[test]
	fn into() {
		let hm_1: HA = SHA1.into();
		let hm_256: HA = SHA256.into();
		let hm_512: HA = SHA512.into();
		assert_eq!(hm_1, HMAC_SHA1_FOR_LEGACY_USE_ONLY);
		assert_eq!(hm_256, HMAC_SHA256);
		assert_eq!(hm_512, HMAC_SHA512);
	}
}
