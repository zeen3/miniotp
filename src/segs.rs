﻿//! Segmentation of base32 secrets.
#[cfg(feature = "alloc")]
use alloc::string::String;
use core::cmp::min;
/// A helper iterator for splitting secrets into more human readable pieces of text.
#[derive(Debug)]
pub struct Segs {
	pub(crate) sec: String,
	pub(crate) chunk_size: usize,
	pub(crate) curr: usize,
}
impl Iterator for Segs {
	type Item = String;

	fn next(&mut self) -> Option<Self::Item> {
		if self.curr >= self.sec.len() {
			None
		} else {
			let (off, ()) = (self.curr, self.curr += self.chunk_size);
			let end = min(self.curr, self.sec.len());
			let next = self.sec.get(off..end)?;
			Some(next.into())
		}
	}
}
impl Segs {
	/// Joins segments into a string.
	///
	/// ```rust
	/// use miniotp::HOTP;
	/// let sec_human = HOTP::new(b"HUMAN").base32_segs(4);
	/// assert_eq!(sec_human.join(" "), "JBKU 2QKO");
	/// ```
	pub fn join<S: AsRef<str>>(
		self,
		sep: S,
	) -> String {
		let s = sep.as_ref();
		let cap = self.sec.len() + s.len() * (self.sec.len() / self.chunk_size) + 1;
		let mut first = true;
		self.fold(String::with_capacity(cap), |mut acc, seg| {
			if first {
				first = false;
			} else {
				acc.push_str(s);
			}
			acc.push_str(&seg);
			acc
		})
	}
}
