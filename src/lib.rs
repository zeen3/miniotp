﻿#![deny(missing_docs, missing_debug_implementations)]
#![cfg_attr(not(feature = "std"), no_std)]
//! # `miniotp`
//!
//! An otp crate that aims for correctness of implementation, while giving the same speed that one
//! would expect.
//!
//! Features:
//!
//! - `std` - on by default; enables use of system time and/or allocations.
//! - `alloc` - off by default; enables use of the [`alloc`](https://doc.rust-lang.org/alloc/)
//!   crate where allocations are required.
//! - `only-gauth` - you only use google authenticator-valid codes. This disables some features,
//!   like setting the length, algorithm, period, or epoch. This is enabled by default.
//! - `cstr` - enables support for creating a code from a `AsRef<CStr>`; for easier compatibility
//!   with [`noria::DataType::Text(_)`](https://docs.rs/noria/0.1.2), or other data bases.
//! - `base32` - on by default; enables a set of functions related to taking an OTP secret as or
//!   into a string.
//! - `serde` - off by default; adds (de)serialisation ability to TOTP and HOTP structs.
//! - `abomonation_impl` - off by default; adds faster, array bytewise (de)serialisation ability to TOTP
//!   and HOTP structs.
//!
#![cfg_attr(
	not(any(
		feature = "std",
		feature = "alloc",
		feature = "only-gauth",
		feature = "cstr",
		feature = "base32",
		feature = "serde",
		feature = "abomonation_impl",
	)),
	doc = "No features were enabled in this documentation build."
)]
#![cfg_attr(
	any(
		feature = "std",
		feature = "alloc",
		feature = "only-gauth",
		feature = "cstr",
		feature = "base32",
		feature = "serde",
		feature = "abomonation_impl"
	),
	doc = "Enabled in this documentation build:\n"
)]
#![cfg_attr(feature = "std", doc = "- `std`")]
#![cfg_attr(feature = "alloc", doc = "- `alloc`")]
#![cfg_attr(
	feature = "only-gauth",
	doc = "- `only-gauth` (disables `Algorithm` type)"
)]
#![cfg_attr(feature = "cstr", doc = "- `cstr`")]
#![cfg_attr(feature = "base32", doc = "- `base32`")]
#![cfg_attr(feature = "serde", doc = "- `serde`")]
#![cfg_attr(feature = "abomonation_impl", doc = "- `abomonation_impl`")]

#[cfg(feature = "abomonation_derive")]
extern crate abomonation_derive;
#[cfg(feature = "alloc")]
extern crate alloc;
#[cfg(all(feature = "base32", any(feature = "std", feature = "alloc")))]
#[cfg_attr(
	docsrs,
	doc(cfg(all(feature = "base32", any(feature = "std", feature = "alloc"))))
)]
pub(crate) const B32A: base32::Alphabet =
	base32::Alphabet::RFC4648 { padding: false };

#[cfg(not(feature = "only-gauth"))]
#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
pub(crate) mod alg;
#[cfg(not(feature = "only-gauth"))]
#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
pub use alg::*;
mod array;
pub use array::Array;
mod hotp;
pub use hotp::HOTP;
mod totp;
pub use totp::TOTP;

#[cfg_attr(docsrs, doc(cfg(all(feature = "std", feature = "only-gauth"))))]
#[cfg(feature = "std")]
/// Simple OTP primitive usage
pub mod simple;
#[cfg(feature = "std")]
#[cfg_attr(docsrs, doc(cfg(all(feature = "std", feature = "only-gauth"))))]
pub use simple::{
	read_hotp_gauth as hotp,
	read_totp_gauth as totp,
};

#[cfg(all(feature = "base32", any(feature = "alloc", feature = "std")))]
mod err;
#[cfg(all(feature = "base32", any(feature = "alloc", feature = "std")))]
#[cfg_attr(
	docsrs,
	doc(cfg(all(feature = "base32", any(feature = "alloc", feature = "std"))))
)]
pub use err::Error;
#[cfg(all(feature = "base32", any(feature = "alloc", feature = "std")))]
mod segs;
#[cfg(all(feature = "base32", any(feature = "alloc", feature = "std")))]
#[cfg_attr(
	docsrs,
	doc(cfg(all(feature = "base32", any(feature = "alloc", feature = "std"))))
)]
pub use segs::Segs;
