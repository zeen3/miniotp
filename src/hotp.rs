﻿use crate::Array;
#[cfg(not(feature = "only-gauth"))]
use crate::*;
#[cfg(feature = "alloc")]
use alloc::{
	format,
	string::String,
};
use ring::hmac::{
	sign,
	Key,
};

#[cfg(feature = "only-gauth")]
use ring::hmac::HMAC_SHA1_FOR_LEGACY_USE_ONLY;
/// A counter-based one-time passcode implementation
#[derive(Debug, Clone, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(
	all(feature = "abomonation_derive", feature = "abomonation_main"),
	derive(abomonation_derive::Abomonation)
)]
pub struct HOTP<A>
where
	A: Array,
{
	/// A fixed-size or vector type of bytes
	pub(crate) sec: A,
	/// The output length of the algorithm
	#[cfg(not(feature = "only-gauth"))]
	#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
	pub(crate) len: u8,
	/// The HMAC algorithm being run
	#[cfg(not(feature = "only-gauth"))]
	#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
	pub(crate) alg: Algorithm,
}

#[cfg(test)]
mod test {
	// Test values come from https://tools.ietf.org/html/rfc4226
	use super::{
		HOTP,
		TRUNCATE_TO,
	};
	const DATA: &[u8] = b"12345678901234567890";
	const OUTPUT: [(u32, u32); 10] = [
		(0x4c93cf18, 755224),
		(0x41397eea, 287082),
		(0x082fef30, 359152),
		(0x66ef7655, 969429),
		(0x61c5938a, 338314),
		(0x33c083d4, 254676),
		(0x7256c032, 287922),
		(0x04e5b397, 162583),
		(0x2823443f, 399871),
		(0x2679dc69, 520489),
	];
	#[test]
	fn counting_works() {
		let hotp = HOTP::new(DATA);
		let z = (0u64..10).into_iter().zip(OUTPUT.iter());
		for (i, (long, expected)) in z {
			let otp = hotp.generate(i);
			assert_eq!(otp, *expected);

			#[cfg(not(feature = "only-gauth"))]
			{
				let otp = hotp.clone().set_len(9).generate(i);
				assert_eq!(otp, long % TRUNCATE_TO[9]);
			}
			#[cfg(feature = "only-gauth")]
			{
				assert_eq!(otp, long % TRUNCATE_TO[6]);
			}
		}
	}
}

#[doc(hidden)]
/// Enables equivalence checking in debug and test builds. Do not rely on this for production code.
impl<A: Array, B: Array> PartialEq<HOTP<B>> for HOTP<A> {
	fn eq(
		&self,
		other: &HOTP<B>,
	) -> bool {
		#[cfg(feature = "only-gauth")]
		let stuff = true;
		#[cfg(not(feature = "only-gauth"))]
		let stuff = self.len == other.len && self.alg == other.alg;
		let all_xor = self
			.sec
			.as_bytes()
			.iter()
			.zip(other.sec.as_bytes())
			.fold(0u8, |v, (a, b)| v | (a ^ b));
		self.sec.as_bytes().len() == other.sec.as_bytes().len()
			&& 0 == all_xor
			&& stuff
	}
}

/// Length to truncate a counter to, by index.
pub const TRUNCATE_TO: [u32; 10] = [
	1,
	10,
	100,
	1_000,
	10_000,
	100_000,
	1_000_000,
	10_000_000,
	100_000_000,
	1_000_000_000,
];
impl<A: Array> HOTP<A> {
	/// Creates a new HOTP algorithm verifier.
	///
	/// ```rust
	/// use miniotp::HOTP;
	/// let hotp = HOTP::new(b"secret A");
	/// let hotp_other = HOTP::new(b"secret B");
	/// # assert_ne!(hotp, hotp_other, "secrets are different");
	/// ```
	pub fn new(sec: A) -> Self {
		#[cfg(not(feature = "only-gauth"))]
		let new = Self {
			sec,
			len: 6,
			alg: SHA1,
		};
		#[cfg(feature = "only-gauth")]
		let new = Self { sec };
		new
	}

	/// Verifies a HOTP counter range from -1 to +1.
	///
	/// This function consumes `self`.
	pub fn verify_range_default(
		self,
		counter: u64,
		input: u32,
	) -> bool {
		self.verify_range(counter, input, -1..=1)
	}

	/// Generates a HOTP value.
	///
	/// ```rust
	/// use miniotp::{HOTP};
	/// let hotp = HOTP::new(b"12345678901234567890");
	/// let otp = hotp.generate(1);
	/// assert_eq!(otp, 287_082);
	/// ```
	pub fn generate(
		&self,
		counter: u64,
	) -> u32 {
		let key = self.sec.as_bytes();

		#[cfg(feature = "only-gauth")]
		let key = Key::new(HMAC_SHA1_FOR_LEGACY_USE_ONLY, key);
		#[cfg(not(feature = "only-gauth"))]
		let key = Key::new(self.alg.into(), key);

		let counter = counter.to_be_bytes();
		let result = sign(&key, &counter);
		let digest = result.as_ref();

		#[cfg(feature = "only-gauth")]
		let last = 19;
		#[cfg(not(feature = "only-gauth"))]
		let last = match self.alg {
			SHA1 => 19,
			SHA256 => 31,
			SHA512 => 63,
		};

		// SAFETY: this is safe as digest always returns a minimum 20 byte slice when using SHA-1.
		let offset = (unsafe { digest.get_unchecked(last) } & 0x0F) as usize;
		// SAFETY: this is safe as offset always satisfies offset @ 0..16.
		let value = u32::from_be_bytes(unsafe {
			[
				0x7F & *digest.get_unchecked(offset),
				*digest.get_unchecked(offset + 1),
				*digest.get_unchecked(offset + 2),
				*digest.get_unchecked(offset + 3),
			]
		});

		#[cfg(feature = "only-gauth")]
		let len = 6;
		#[cfg(not(feature = "only-gauth"))]
		let len = self.len as usize;

		value % TRUNCATE_TO[len]
	}

	/// Verifies a HOTP value.
	///
	/// ```rust
	/// use miniotp::{HOTP};
	/// let hotp = HOTP::new(b"12345678901234567890");
	/// let otp = hotp.verify(349495, 000_000);
	/// assert_eq!(otp, true);
	/// ```
	pub fn verify(
		&self,
		counter: u64,
		input: u32,
	) -> bool {
		let value = self.generate(counter);
		// input xor value must always return zero, or it is not correct
		(input ^ value) == 0
	}

	/// Verifies a HOTP counter range.
	///
	/// This function consumes `self`.
	pub fn verify_range<I>(
		self,
		counter: u64,
		input: u32,
		diffs: I,
	) -> bool
	where
		I: IntoIterator<Item = i64>,
	{
		let mut ok = false;
		for diff in diffs {
			let new_count = if diff.is_negative() {
				counter + (diff.abs() as u64)
			} else {
				counter - diff as u64
			};
			ok |= self.verify(new_count, input);
		}
		ok
	}
}
#[cfg(not(feature = "only-gauth"))]
#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
impl<A: Array> HOTP<A> {
	/// Sets the algorithm.
	///
	/// ```rust
	/// use miniotp::{HOTP, SHA1, SHA256, SHA512};
	/// let hotp_default = HOTP::new(b"secret");
	/// let hotp_sha1 = HOTP::new(b"secret").set_alg(SHA1);
	/// # assert_eq!(hotp_default, hotp_sha1, "default algorithm is sha1");
	/// let hotp_sha256 = HOTP::new(b"secret").set_alg(SHA256);
	/// let hotp_sha512 = HOTP::new(b"secret").set_alg(SHA512);
	/// # assert_ne!(hotp_sha256, hotp_sha512, "sha256 and sha512 are different algs");
	/// # dbg!((hotp_sha1, hotp_sha256, hotp_sha512));
	/// ```
	pub fn set_alg(
		mut self,
		alg: crate::alg::Algorithm,
	) -> Self {
		self.alg = alg;
		self
	}

	/// Sets the output length.
	///
	/// ```rust
	/// use miniotp::{HOTP};
	/// let hotp_default = HOTP::new(b"secret");
	/// let hotp_len_6 = HOTP::new(b"secret").set_len(6);
	/// # assert_eq!(hotp_default, hotp_len_6, "default len is 6");
	/// let hotp_len_7 = HOTP::new(b"secret").set_len(7);
	/// let hotp_len_8 = HOTP::new(b"secret").set_len(8);
	/// # dbg!((hotp_len_6, hotp_len_7, hotp_len_8));
	/// ```
	pub fn set_len(
		mut self,
		len: u8,
	) -> Self {
		self.len = len;
		self
	}
}
#[cfg(all(feature = "base32", any(feature = "std", feature = "alloc")))]
mod b32 {
	use crate::{
		Array,
		Error,
		B32A,
		HOTP,
	};
	#[cfg(feature = "alloc")]
	use alloc::{
		format,
		string::String,
		vec::Vec,
	};
	#[cfg(feature = "cstr")]
	use std::ffi::CStr;
	#[cfg_attr(
		docsrs,
		doc(cfg(cfg(all(
			feature = "base32",
			any(feature = "std", feature = "alloc")
		))))
	)]
	impl<A: Array> HOTP<A> {
		/// Returns the base32 encoded equivalent of the internal secret.
		///
		/// ```rust
		/// use miniotp::HOTP;
		/// let sec_human = HOTP::new(b"HUMAN").base32_secret();
		/// assert_eq!(sec_human, "JBKU2QKO");
		/// ```
		pub fn base32_secret(&self) -> String {
			base32::encode(B32A, self.sec.as_bytes())
		}

		/// Returns an iterator over base32 segments of `chunk` size.
		pub fn base32_segs(
			&self,
			chunk_size: usize,
		) -> crate::Segs {
			let sec = self.base32_secret();
			crate::Segs {
				sec,
				chunk_size,
				curr: 0,
			}
		}

		/// Generates a URI for this HOTP authenticator, given a label and issuer.
		pub fn to_uri<S: AsRef<str>>(
			&self,
			label: S,
			issuer: S,
			counter: u64,
		) -> String {
			#[cfg(feature = "only-gauth")]
			let (alg, len) = ("SHA1", 6);
			#[cfg(not(feature = "only-gauth"))]
			let (alg, len) = (self.alg, self.len);
			format!(
				"otpauth://hotp/{lbl}?secret={sec}&issuer={iss}&counter={cnt}&algorithm={alg}&digits={len}",
				sec = self.base32_secret(),
				lbl = label.as_ref(),
				iss = issuer.as_ref(),
				cnt = counter,
				alg = alg,
				len = len
			)
		}
	}
	impl HOTP<Vec<u8>> {
		/// Method to take a base32 string and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::HOTP;
		/// let otp = HOTP::from_base32("AAAAAAAA")?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		pub fn from_base32<S: AsRef<str>>(s: S) -> Result<Self, Error> {
			let s = s.as_ref();
			base32::decode(B32A, s)
				.map(|vec| Self::new(vec))
				.ok_or_else(|| Error::BadEnc)
		}

		/// Method to take a base32 cstr and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::HOTP;
		/// use std::ffi::CString;
		/// let cs = CString::new("AAAAAAAA").expect("cstr");
		/// let otp = HOTP::from_base32c(cs)?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		#[cfg(feature = "cstr")]
		#[cfg_attr(
			docsrs,
			doc(cfg(cfg(all(
				feature = "base32",
				feature = "cstr",
				any(feature = "std", feature = "alloc")
			))))
		)]
		pub fn from_base32c<C: AsRef<CStr>>(s: C) -> Result<Self, Error> {
			let s = s.as_ref().to_str()?;
			base32::decode(B32A, s)
				.ok_or_else(|| Error::BadEnc)
				.map(|v| Self::new(v))
		}

		/// Method to take a base32 cstr-like and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::HOTP;
		/// let otp = HOTP::from_base32b(b"AAAAAAAA\0")?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		pub fn from_base32b<'a, I: IntoIterator<Item = &'a u8>>(
			i: I
		) -> Result<Self, Error> {
			let key: Vec<u8> = i
				.into_iter()
				.copied()
				.take_while(u8::is_ascii_graphic)
				.collect();
			// SAFETY: the key above is always valid ascii. This is actually safe.
			let key = unsafe { String::from_utf8_unchecked(key) };
			Self::from_base32(key)
		}
	}
}
#[cfg(any(feature = "std", feature = "alloc"))]
#[cfg_attr(docsrs, doc(cfg(cfg(any(feature = "std", feature = "alloc")))))]
impl<A: Array> HOTP<A> {
	/// Generates the value as a string.
	///
	/// ```rust
	/// use miniotp::HOTP;
	/// let s = HOTP::new(b"12345678901234567890").generate_str(349495);
	/// assert_eq!(s, "000000");
	/// ```
	pub fn generate_str(
		&self,
		counter: u64,
	) -> String {
		let value = self.generate(counter);
		#[cfg(feature = "only-gauth")]
		let len = 6;
		#[cfg(not(feature = "only-gauth"))]
		let len = self.len as usize;
		format!("{v:0<len$}", len = len, v = value)
	}
}
