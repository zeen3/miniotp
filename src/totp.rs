﻿use crate::{
	Array,
	HOTP,
};
#[cfg(feature = "alloc")]
use alloc::{
	format,
	string::String,
};

/// A time-based one-time passcode implementation
#[derive(Debug, Clone, PartialEq, Copy)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[cfg_attr(
	all(feature = "abomonation_derive", feature = "abomonation_main"),
	derive(abomonation_derive::Abomonation)
)]
pub struct TOTP<A>
where
	A: Array,
{
	/// The internal TOTP state. See its documentation for more.
	hotp: HOTP<A>,
	#[cfg(not(feature = "only-gauth"))]
	#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
	/// The epoch of the TOTP implementation, in seconds. Almost always zero.
	epoch: u64,
	#[cfg(not(feature = "only-gauth"))]
	#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
	/// The period of the TOTP implementation, in seconds.
	period: u64,
}

impl<A: Array> From<HOTP<A>> for TOTP<A> {
	fn from(hotp: HOTP<A>) -> Self {
		#[cfg(not(feature = "only-gauth"))]
		let new = Self {
			hotp,
			epoch: 0,
			period: 30,
		};
		#[cfg(feature = "only-gauth")]
		let new = Self { hotp };
		new
	}
}
#[cfg(all(feature = "abomonation_derive", feature = "abomonation_main", test))]
mod abomonation {
	use super::*;
	use ::abomonation_main::*;
	#[cfg(feature = "alloc")]
	use alloc::vec::Vec;
	use std::{
		error::Error,
		fmt::{
			self,
			Display,
			Formatter as F,
		},
	};
	#[derive(Debug, Clone)]
	struct FailedToRestore;
	impl Display for FailedToRestore {
		fn fmt(
			&self,
			f: &mut F,
		) -> fmt::Result {
			f.write_str("Failed to restore")
		}
	}
	impl Error for FailedToRestore {}
	#[test]
	fn abomonate() -> Result<(), Box<dyn Error>> {
		let default = TOTP::new([0; 8]);
		let mut data = Vec::with_capacity(measure(&default));
		unsafe { encode(&default, &mut data)? };
		dbg!(&data);
		if let Some((restored, &mut [])) =
			unsafe { decode::<TOTP<[u8; 8]>>(&mut data) }
		{
			assert_eq!(default, *restored);
			Ok(())
		} else {
			Err(Box::new(FailedToRestore))
		}
	}
}

// this hacky test doesn't work without std or alloc.
#[cfg(all(test, any(feature = "alloc", feature = "std")))]
mod test {
	// test values come from https://tools.ietf.org/html/rfc6238#appendix-B
	use crate::*;
	#[cfg(feature = "alloc")]
	use alloc::vec::Vec;
	const SHA1_VALUES: [(u64, u32); 6] = [
		(59, 94_287_082),
		(1111111109, 7_081_804),
		(1111111111, 14_050_471),
		(1234567890, 89_005_924),
		(2000000000, 69_279_037),
		(20000000000, 65_353_130),
	];

	#[cfg(not(feature = "only-gauth"))]
	const VALUES: [(Algorithm, [(u64, u32); 6]); 3] = [
		(SHA1, SHA1_VALUES),
		(SHA256, [
			(59, 46_119_246),
			(1111111109, 68_084_774),
			(1111111111, 67_062_674),
			(1234567890, 91_819_424),
			(2000000000, 90_698_825),
			(20000000000, 77_737_706),
		]),
		(SHA512, [
			(59, 90_693_936),
			(1111111109, 25_091_201),
			(1111111111, 99_943_326),
			(1234567890, 93_441_116),
			(2000000000, 38_618_901),
			(20000000000, 47_863_826),
		]),
	];
	#[test]
	fn works_with_std_or_alloc() {
		#[cfg(feature = "only-gauth")]
		let values = Some(((), SHA1_VALUES)).iter();
		#[cfg(not(feature = "only-gauth"))]
		let values = VALUES.iter();
		for (alg, values) in values {
			#[cfg(not(feature = "only-gauth"))]
			let len = match alg {
				SHA1 => 20,
				SHA256 => 32,
				SHA512 => 64,
			};
			#[cfg(feature = "only-gauth")]
			let len = 20;
			let sec: Vec<u8> = (b'0'..=b'9')
				.into_iter()
				.cycle()
				.skip(1)
				.take(len)
				.collect();
			#[cfg(feature = "std")]
			dbg!(String::from_utf8(sec.clone()).unwrap());
			#[cfg(not(feature = "only-gauth"))]
			let totp = TOTP::new(sec).set_alg(*alg).set_len(8);
			#[cfg(feature = "only-gauth")]
			let totp = TOTP::new(sec);
			for (time, expected) in values.iter() {
				#[cfg(feature = "only-gauth")]
				let expected = &(expected % 1_000_000);
				let otp = totp.generate(*time);
				assert_eq!(otp, *expected, "{:?} @ {} fail", alg, time);
				#[cfg(feature = "std")]
				dbg!((otp, expected, alg, time));
			}
		}
	}
}

impl<A: Array> TOTP<A> {
	/// Creates a new TOTP algorithm verifier.
	///
	/// ```rust
	/// use miniotp::TOTP;
	/// let totp = TOTP::new(b"secret A");
	/// let totp_other = TOTP::new(b"secret B");
	/// # assert_ne!(totp, totp_other, "secrets are different");
	/// ```
	pub fn new(a: A) -> Self {
		HOTP::new(a).into()
	}

	/// Verifies a TOTP range from -1 to +1.
	///
	/// This function consumes `self`.
	pub fn verify_range_default(
		self,
		time: u64,
		input: u32,
	) -> bool {
		self.verify_range(time, input, -1..=1)
	}

	/// Generates a HOTP value.
	///
	/// ```rust
	/// use miniotp::{TOTP};
	/// let totp = TOTP::new(b"12345678901234567890");
	/// let gauthkey = totp.generate(30);
	/// assert_eq!(gauthkey, 287_082);
	/// ```
	pub fn generate(
		&self,
		time: u64,
	) -> u32 {
		#[cfg(feature = "only-gauth")]
		let time = time / 30;
		#[cfg(not(feature = "only-gauth"))]
		let time = (time - self.epoch) / self.period;
		self.hotp.generate(time)
	}

	/// Verifies a TOTP value.
	///
	/// ```rust
	/// use miniotp::{TOTP};
	/// let totp = TOTP::new(b"12345678901234567890");
	/// let otp = totp.verify(349495 * 30, 000_000);
	/// assert_eq!(otp, true);
	/// ```
	pub fn verify(
		&self,
		time: u64,
		input: u32,
	) -> bool {
		let value = self.generate(time);
		// input xor value must always return zero, or it is not correct
		(input ^ value) == 0
	}

	/// Verifies a TOTP time range.
	///
	/// This function consumes `self`.
	pub fn verify_range<I>(
		self,
		time: u64,
		input: u32,
		diffs: I,
	) -> bool
	where
		I: IntoIterator<Item = i64>,
	{
		let mut ok = false;
		#[cfg(not(feature = "only-gauth"))]
		let df = |v| self.period * v;
		#[cfg(feature = "only-gauth")]
		let df = |v| 30 * v;
		for diff in diffs {
			let new_count = if diff.is_negative() {
				time + df(diff.abs() as u64)
			} else {
				time - df(diff as u64)
			};
			ok |= self.verify(new_count, input);
		}
		ok
	}
}

#[cfg(feature = "std")]
#[cfg_attr(docsrs, doc(cfg(feature = "std")))]
impl<A: Array> TOTP<A> {
	/// Verifies a TOTP range at the current time with a range of -1..=+1 period.
	///
	/// This function consumes `self`.
	///
	/// ```rust
	/// use miniotp::TOTP;
	/// let totp = TOTP::new(b"AAAAAAAA").verify_range_default_now(123);
	/// # use core::any::Any;
	/// # assert_eq!(totp.type_id(), true.type_id());
	/// ```
	///
	/// ### Panics
	///
	/// This function will panic if system time is less than the unix epoch.
	pub fn verify_range_default_now(
		self,
		input: u32,
	) -> bool {
		use std::time::*;
		let time = SystemTime::now()
			.duration_since(SystemTime::UNIX_EPOCH)
			.expect("System time to be greater than the unix epoch")
			.as_secs();
		self.verify_range_default(time, input)
	}

	/// Generates a TOTP value at the current time.
	///
	/// ### Panics
	///
	/// This function will panic if system time is less than the unix epoch.
	pub fn generate_now(&self) -> u32 {
		use std::time::*;
		let time = SystemTime::now()
			.duration_since(SystemTime::UNIX_EPOCH)
			.expect("System time to be greater than the unix epoch")
			.as_secs();
		self.generate(time)
	}
}

#[cfg(not(feature = "only-gauth"))]
#[cfg_attr(docsrs, doc(cfg(not(feature = "only-gauth"))))]
impl<A: Array> TOTP<A> {
	/// Sets the algorithm.
	///
	/// This functions the same as [`HOTP::set_alg`](./struct.HOTP.html#method.set_alg);
	/// see its documentation for more.
	pub fn set_alg(
		mut self,
		alg: crate::alg::Algorithm,
	) -> Self {
		self.hotp.alg = alg;
		self
	}

	/// Sets the output length.
	///
	/// This functions the same as [`HOTP::set_len`](./struct.HOTP.html#method.set_len);
	/// see its documentation for more.
	pub fn set_len(
		mut self,
		len: u8,
	) -> Self {
		self.hotp.len = len;
		self
	}

	/// Sets the period, in seconds.
	///
	/// ```rust
	/// use miniotp::TOTP;
	/// // use a 53 second period for fairly unaligned intervals
	/// let otp = TOTP::new(b"secret")
	///   .set_period(53)
	///   .generate(3);
	/// # dbg!(otp);
	/// ```
	pub fn set_period(
		mut self,
		period: u64,
	) -> Self {
		self.period = period;
		self
	}

	/// Sets the epoch, in seconds.
	///
	/// ```rust
	/// use miniotp::TOTP;
	/// // use 2000-01-01T00:00:00Z as your epoch
	/// let otp = TOTP::new(b"secret")
	///   .set_epoch(946_684_800)
	///   .generate(1_577_836_800);
	/// ```
	///
	/// ### Panics
	///
	/// This function can cause a panic if `generate` or `generate_now` is given a time before the
	/// epoch.
	pub fn set_epoch(
		mut self,
		epoch: u64,
	) -> Self {
		self.epoch = epoch;
		self
	}
}

#[cfg(all(feature = "base32", any(feature = "std", feature = "alloc")))]
pub mod b32 {
	use crate::{
		Array,
		Error,
		HOTP,
		TOTP,
	};
	#[cfg(feature = "alloc")]
	use alloc::{
		format,
		string::String,
		vec::Vec,
	};
	#[cfg(feature = "cstr")]
	use std::ffi::CStr;
	#[cfg_attr(
		docsrs,
		doc(cfg(cfg(all(
			feature = "base32",
			any(feature = "std", feature = "alloc")
		))))
	)]
	impl<A: Array> TOTP<A> {
		/// Returns the base32 encoded equivalent of the internal secret.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::TOTP;
		/// let totp = TOTP::new(b"\0\x01\x02\x03\x04");
		/// let b32 = totp.base32_secret();
		/// assert_eq!(b32, "AAAQEAYE");
		/// # Ok(()) }
		/// ```
		pub fn base32_secret(&self) -> String {
			self.hotp.base32_secret()
		}

		/// Returns an iterator over a the encoded secret of a string of `chunk` size.
		pub fn base32_segs(
			&self,
			chunk: usize,
		) -> crate::Segs {
			self.hotp.base32_segs(chunk)
		}

		/// Generates a URI for this TOTP authenticator, given a label and issuer.
		pub fn to_uri<S: AsRef<str>>(
			&self,
			label: S,
			issuer: S,
		) -> String {
			#[cfg(feature = "only-gauth")]
			let (alg, len, per, epoch) = {
				#[derive(Debug)]
				struct SHA1;
				(SHA1, 6, 30, 0)
			};
			#[cfg(not(feature = "only-gauth"))]
			let (alg, len, per, epoch) =
				(self.hotp.alg, self.hotp.len, self.period, self.epoch);
			format!(
				"otpauth://totp/{lbl}?secret={sec}&issuer={iss}&algorithm={alg:?}&digits={len}&period={per}&epoch={T0}",
				sec = self.base32_secret(),
				lbl = label.as_ref(),
				iss = issuer.as_ref(),
				alg = alg,
				len = len,
				per = per,
				T0 = epoch
			)
		}
	}
	impl TOTP<Vec<u8>> {
		/// Method to take a base32 string and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::{TOTP};
		/// let otp = TOTP::from_base32("AAAAAAAA")?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		pub fn from_base32<S: AsRef<str>>(s: S) -> Result<Self, Error> {
			HOTP::from_base32(s).map(Into::into)
		}

		/// Method to take a base32 cstr and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::TOTP;
		/// use std::ffi::CString;
		/// let cs = CString::new("AAAAAAAA").expect("cstr");
		/// let otp = TOTP::from_base32c(cs)?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		#[cfg(feature = "cstr")]
		#[cfg_attr(
			docsrs,
			doc(cfg(cfg(all(
				feature = "base32",
				feature = "cstr",
				any(feature = "std", feature = "alloc")
			))))
		)]
		pub fn from_base32c<C: AsRef<CStr>>(s: C) -> Result<Self, Error> {
			HOTP::from_base32c(s).map(Into::into)
		}

		/// Method to take a base32 cstr-like and use it as an inital vector.
		///
		/// ```rust
		/// # fn main() -> Result<(), miniotp::Error> {
		/// use miniotp::TOTP;
		/// let otp = TOTP::from_base32b(b"AAAAAAAA\0")?.generate(3);
		/// # dbg!(otp);
		/// # assert!(otp < u32::MAX);
		/// # Ok(())
		/// # }
		/// ```
		pub fn from_base32b<'a, I: IntoIterator<Item = &'a u8>>(
			iter: I
		) -> Result<Self, Error> {
			HOTP::from_base32b(iter).map(Into::into)
		}
	}
}
#[cfg(any(feature = "std", feature = "alloc"))]
#[cfg_attr(docsrs, doc(cfg(cfg(any(feature = "std", feature = "alloc")))))]
impl<A: Array> TOTP<A> {
	/// Generates the value as a string.
	///
	/// ```rust
	/// use miniotp::TOTP;
	/// let s = TOTP::new(b"12345678901234567890").generate_str(59);
	/// assert_eq!(s, "287082");
	/// ```
	pub fn generate_str(
		&self,
		time: u64,
	) -> String {
		let value = self.generate(time);
		#[cfg(feature = "only-gauth")]
		let len = 6;
		#[cfg(not(feature = "only-gauth"))]
		let len = self.hotp.len as usize;
		format!("{v:0<len$}", len = len, v = value)
	}
}
