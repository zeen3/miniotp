﻿//! Errors for a few subsets of errors
#[cfg(feature = "cstr")]
use std::str::Utf8Error;
/// Mapping error type.
#[derive(Debug)]
pub enum Error {
	#[cfg(feature = "cstr")]
	/// UTF-8 encoding error
	///
	/// Only returned when using a cstring.
	Utf8(Utf8Error),
	/// Bad base32 encoding
	BadEnc,
}
impl core::fmt::Display for Error {
	fn fmt(
		&self,
		f: &mut core::fmt::Formatter,
	) -> core::fmt::Result {
		match self {
			#[cfg(feature = "cstr")]
			Self::Utf8(ref e) => core::fmt::Display::fmt(e, f),
			Self::BadEnc => f.write_str("Bad base32 encoding"),
		}
	}
}
#[cfg(feature = "cstr")]
impl From<Utf8Error> for Error {
	fn from(e: Utf8Error) -> Self {
		Self::Utf8(e)
	}
}
#[cfg(feature = "std")]
impl std::error::Error for Error {}
