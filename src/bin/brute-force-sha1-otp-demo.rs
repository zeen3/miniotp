﻿use ring::hmac::{
	sign,
	Key,
	HMAC_SHA1_FOR_LEGACY_USE_ONLY,
};
pub fn generate(
	key: &Key,
	counter: u64,
) -> u32 {
	let counter = counter.to_be_bytes();
	let result = sign(key, &counter);
	let digest = result.as_ref();

	let last = 19;

	// SAFETY: this is safe as digest always returns a minimum 20 byte slice when using SHA-1.
	let offset = (unsafe { digest.get_unchecked(last) } & 0x0F) as usize;
	// SAFETY: this is safe as offset always satisfies offset @ 0..16.
	let value = u32::from_be_bytes(unsafe {
		[
			0x7F & *digest.get_unchecked(offset),
			*digest.get_unchecked(offset + 1),
			*digest.get_unchecked(offset + 2),
			*digest.get_unchecked(offset + 3),
		]
	});

	value % 1_000_000
}
use std::{
	env::args,
	thread::yield_now,
	time::Instant,
};

fn main() {
	let time = Instant::now();
	let target: u32 = args()
		.nth(1)
		.map(|v| v.parse().unwrap_or_default())
		.unwrap_or_default();
	let key: Vec<u8> = args()
		.nth(2)
		.map(|v| v.as_bytes().to_owned())
		.unwrap_or("12345678901234567890".as_bytes().to_owned());
	println!(
		"Working towards a target of {} using a key of {:?}.",
		target, key
	);
	let key = Key::new(HMAC_SHA1_FOR_LEGACY_USE_ONLY, &key);
	for i in 0.. {
		if i & 0x1F_FF == 0 {
			println!("Progress: {} (elapsed: {:.3?})", i, time.elapsed());
			yield_now();
		}
		let v = generate(&key, i);
		if v == target {
			println!("Target {} reached in {:.3?}: {}", target, time.elapsed(), i);
			break;
		}
	}
}
